package com.cwise.driverlivestream;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    public static final String MY_PREF = "my pref";
    private TextInputLayout email, password;
    private Button btnlogin;
    public static String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        email = findViewById(R.id.loginemail);
        password = findViewById(R.id.loginpassword);
        btnlogin = findViewById(R.id.loginbutton);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailtext = email.getEditText().getText().toString();
                String passwordtext = password.getEditText().getText().toString();
                if (TextUtils.isEmpty(emailtext)) {
                    email.setError("enter your email");
                    return;
                } else {
                    email.setError(null);
                }
                if (TextUtils.isEmpty(passwordtext)) {
                    password.setError("enter your password");
                    return;
                } else {
                    password.setError(null);
                }
                if (passwordtext.length() < 8) {
                    password.setError("password too short");
                    return;
                } else {
                    password.setError(null);
                }
                if (!emailtext.matches(emailpattern)) {
                    email.setError("invalid email");
                    return;
                } else {
                    email.setError(null);
                }
                UserSession user = new UserSession(emailtext, passwordtext);
                AuthSession authSession = new AuthSession(LoginActivity.this);
                authSession.saveSession(user);
                loginUser(emailtext, passwordtext);
            }
        });
    }

    private void loginUser(String email, String password) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        LoginModel loginModel = new LoginModel(email, password);
        Call<LoginRes> call = apiPost.loginUser(loginModel);
        call.enqueue(new Callback<LoginRes>() {
            @Override
            public void onResponse(Call<LoginRes> call, Response<LoginRes> response) {
                if (response.isSuccessful()) {
                    LoginRes resObj = response.body();
                    List<User> list = Collections.singletonList(resObj.getUser());
                    int driverid = list.get(0).getId();
                    String drivername = list.get(0).getName();

                    SharedPreferences preferences = getSharedPreferences(MY_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("driverIDKey", String.valueOf(driverid));
                    editor.putString("name", drivername);
                    editor.apply();

                    if (resObj.getMessage().equals("Signed in successfully")) {
                        Loading.dismiss();
                        Toast.makeText(LoginActivity.this, "Logged in Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(LoginActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }


}