package com.cwise.driverlivestream;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripTimeDatum {
    @SerializedName("driver_id")
    @Expose
    private String driver_id;
    @SerializedName("trip_name")
    @Expose
    private String trip_name;

    public TripTimeDatum(String driver_id, String trip_name) {
        this.driver_id = driver_id;
        this.trip_name = trip_name;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name) {
        this.trip_name = trip_name;
    }
}
