package com.cwise.driverlivestream;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("user_type")
        @Expose
        private String userType;
        @SerializedName("email_verified_at")
        @Expose
        private Object emailVerifiedAt;
        @SerializedName("fcm_token")
        @Expose
        private Object fcmToken;
        @SerializedName("auth_token")
        @Expose
        private String authToken;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("childs_name")
        @Expose
        private Object childsName;
        @SerializedName("childs_age")
        @Expose
        private Object childsAge;
        @SerializedName("childs_gender")
        @Expose
        private Object childsGender;
        @SerializedName("childs_grade")
        @Expose
        private Object childsGrade;
        @SerializedName("Schools_name")
        @Expose
        private Object schoolsName;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("pickup_lat")
        @Expose
        private Object pickupLat;
        @SerializedName("pickup_lng")
        @Expose
        private Object pickupLng;
        @SerializedName("dropoff_lat")
        @Expose
        private Object dropoffLat;
        @SerializedName("dropoff_lng")
        @Expose
        private Object dropoffLng;
        @SerializedName("payment_status")
        @Expose
        private Integer paymentStatus;
        @SerializedName("block_status")
        @Expose
        private Integer blockStatus;
        @SerializedName("qr_code")
        @Expose
        private Object qrCode;
        @SerializedName("childs_image")
        @Expose
        private Object childsImage;
        @SerializedName("driver_lat")
        @Expose
        private Object driverLat;
        @SerializedName("driver_long")
        @Expose
        private Object driverLong;


        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public Object getEmailVerifiedAt() {
            return emailVerifiedAt;
        }

        public void setEmailVerifiedAt(Object emailVerifiedAt) {
            this.emailVerifiedAt = emailVerifiedAt;
        }

        public Object getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(Object fcmToken) {
            this.fcmToken = fcmToken;
        }

        public String getAuthToken() {
            return authToken;
        }

        public void setAuthToken(String authToken) {
            this.authToken = authToken;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getChildsName() {
            return childsName;
        }

        public void setChildsName(Object childsName) {
            this.childsName = childsName;
        }

        public Object getChildsAge() {
            return childsAge;
        }

        public void setChildsAge(Object childsAge) {
            this.childsAge = childsAge;
        }

        public Object getChildsGender() {
            return childsGender;
        }

        public void setChildsGender(Object childsGender) {
            this.childsGender = childsGender;
        }

        public Object getChildsGrade() {
            return childsGrade;
        }

        public void setChildsGrade(Object childsGrade) {
            this.childsGrade = childsGrade;
        }

        public Object getSchoolsName() {
            return schoolsName;
        }

        public void setSchoolsName(Object schoolsName) {
            this.schoolsName = schoolsName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public Object getPickupLat() {
            return pickupLat;
        }

        public void setPickupLat(Object pickupLat) {
            this.pickupLat = pickupLat;
        }

        public Object getPickupLng() {
            return pickupLng;
        }

        public void setPickupLng(Object pickupLng) {
            this.pickupLng = pickupLng;
        }

        public Object getDropoffLat() {
            return dropoffLat;
        }

        public void setDropoffLat(Object dropoffLat) {
            this.dropoffLat = dropoffLat;
        }

        public Object getDropoffLng() {
            return dropoffLng;
        }

        public void setDropoffLng(Object dropoffLng) {
            this.dropoffLng = dropoffLng;
        }

        public Integer getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(Integer paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public Integer getBlockStatus() {
            return blockStatus;
        }

        public void setBlockStatus(Integer blockStatus) {
            this.blockStatus = blockStatus;
        }

        public Object getQrCode() {
            return qrCode;
        }

        public void setQrCode(Object qrCode) {
            this.qrCode = qrCode;
        }

        public Object getChildsImage() {
            return childsImage;
        }

        public void setChildsImage(Object childsImage) {
            this.childsImage = childsImage;
        }

        public Object getDriverLat() {
            return driverLat;
        }

        public void setDriverLat(Object driverLat) {
            this.driverLat = driverLat;
        }

        public Object getDriverLong() {
            return driverLong;
        }

        public void setDriverLong(Object driverLong) {
            this.driverLong = driverLong;
        }

    }
