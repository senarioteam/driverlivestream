package com.cwise.driverlivestream;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainLiveActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_live);
    }
}