package com.cwise.driverlivestream;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiPost {


    @POST("driver_sign_in")
    Call<LoginRes> loginUser(@Body LoginModel loginModel);

    @FormUrlEncoded
    @POST("trip_name")
    Call<TripTimeRes> tripTime(@Field("driver_id") String driver_id,
                                              @Field("trip_name") String tripName);

}
